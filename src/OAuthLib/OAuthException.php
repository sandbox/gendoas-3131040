<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\OAuthLib\OAuthException.
 */

namespace Drupal\sxt_oauth\OAuthLib;

/**
 *  Generic oauth exception class
 */
class OAuthException extends \Exception {
	// pass
}
