<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\Entity\XtOAuthAccepted.
 */

namespace Drupal\sxt_oauth\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityBase;

/**
 * Defines the oauth client entity.
 *
 * @ContentEntityType(
 *   id = "xtouth_accepted",
 *   label = @Translation("OAuth accepted"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_oauth\XtOAuthAcceptedListBuilder",
 *     "form" = {
 *       "default" = "Drupal\sxt_oauth\Form\XtOAuthAcceptedForm",
 *       "delete" = "Drupal\sxt_oauth\Form\XtOAuthAcceptedDeleteForm",
 *       "edit" = "Drupal\sxt_oauth\Form\XtOAuthAcceptedForm"
 *     },
 *   },
 *   base_table = "xtouth_accepted",
 *   entity_keys = {
 *     "id" = "cid"
 *   }
 * )
 */
class XtOAuthAccepted extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['cid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Mail confirmed'))
        ->setDescription(t('Consumer id of the registered oauth customer.'))
        ->setRequired(TRUE);

    $fields['access_token'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Access token'))
        ->setDescription(t('OAuth access token built for consumer.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ]);

    $fields['access_secret'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Access secret'))
        ->setDescription(t('OAuth access secret key built for consumer.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'disabled' => TRUE,
      'weight' => 10,
    ]);

    $fields['date_accepted'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Accepted'))
        ->setDescription(t('The date on which the request has been accepted.'));

    return $fields;
  }

}
