<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\Entity\XtOAuthConsumer.
 */

namespace Drupal\sxt_oauth\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the oauth client entity.
 *
 * @ContentEntityType(
 *   id = "xtouth_consumer",
 *   label = @Translation("OAuth consumer"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_oauth\XtOAuthConsumerListBuilder",
 *     "form" = {
 *       "default" = "Drupal\sxt_oauth\Form\XtOAuthConsumerForm",
 *       "delete" = "Drupal\sxt_oauth\Form\XtOAuthConsumerDeleteForm",
 *       "edit" = "Drupal\sxt_oauth\Form\XtOAuthConsumerForm"
 *     },
 *   },
 *   base_table = "xtouth_consumer",
 *   entity_keys = {
 *     "id" = "cid",
 *     "label" = "name",
 *     "stage" = "stage"
 *   }
 * )
 */
class XtOAuthConsumer extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['cid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('ID'))
        ->setDescription(t('The OAuth customer ID.'))
        ->setReadOnly(TRUE)
        ->setSetting('unsigned', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Name'))
        ->setDescription(t('The name of the consumer site.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 255)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ]);

    $fields['description'] = BaseFieldDefinition::create('string_long')
        ->setLabel(t('Description'))
        ->setDescription(t('The description of the consumer site.'))
                ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => 10,
    ]);


    $fields['version'] = BaseFieldDefinition::create('string')
        ->setLabel(t('OAuth version'))
        ->setDescription(t('The OAuth version used by consumer.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 20)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'size' => 20,
      'weight' => 20,
    ]);

    $fields['mail'] = BaseFieldDefinition::create('email')
        ->setLabel(t("E-Mail"))
        ->setDescription(t('The email of the consumer site.'))
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 30,
    ]);

    $fields['mail_confirmed'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Mail confirmed'))
        ->setDescription(t('The date on which the email was confirmed.'));

    $fields['consumer_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Consumer key'))
        ->setDescription(t('OAuth consumer key.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
                ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 40,
    ]);


    $fields['secret_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Consumer secret key'))
        ->setDescription(t('OAuth consumer secret key.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
                ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 50,
    ]);


    $fields['rsa_key'] = BaseFieldDefinition::create('string_long')
        ->setLabel(t('Public rsa key'))
        ->setDescription(t('The public rsa key.'))
                ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => 60,
    ]);


    $fields['stage'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Stage'))
        ->setDescription(t('Current stages of the registered consumer.'))
        ->setRequired(TRUE);

    $fields['stage_changed'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Stage changed'))
        ->setDescription(t('The date on which the stage has been last changed.'));

    $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Registered on'))
        ->setDescription(t('The date on which the site has been registered.'));

    $fields['Last change'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Last change'))
        ->setDescription(t('The date of the last change.'));

    return $fields;
  }

}
