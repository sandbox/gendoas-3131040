<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\Entity\XtOAuthClient.
 */

namespace Drupal\sxt_oauth\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the oauth client entity.
 *
 * @ContentEntityType(
 *   id = "xtouth_client",
 *   label = @Translation("OAuth client"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_oauth\XtOAuthClientListBuilder",
 *     "form" = {
 *       "default" = "Drupal\sxt_oauth\Form\XtOAuthClientForm",
 *       "delete" = "Drupal\sxt_oauth\Form\XtOAuthClientDeleteForm",
 *       "edit" = "Drupal\sxt_oauth\Form\XtOAuthClientForm"
 *     },
 *   },
 *   base_table = "xtouth_client",
 *   entity_keys = {
 *     "id" = "clid",
 *     "label" = "name"
 *   }
 * )
 */
class XtOAuthClient extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['clid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('ID'))
        ->setDescription(t('The OAuth client ID.'))
        ->setReadOnly(TRUE)
        ->setSetting('unsigned', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Name'))
        ->setDescription(t('Short descriptive name of the server site.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 255)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
        ]);

    $fields['url'] = BaseFieldDefinition::create('uri')
        ->setLabel(t('URL'))
        ->setDescription(t('URL of the server site where to access contents.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 255)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 10,
        ]);

    $fields['consumer_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Consumer key'))
        ->setDescription(t('OAuth consumer key.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 20,
        ]);

    $fields['secret_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Consumer secret key'))
        ->setDescription(t('OAuth consumer secret key.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 30,
        ]);

    $fields['access_token'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Access token'))
        ->setDescription(t('OAuth access token.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 40,
        ]);

    $fields['access_secret'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Access secret'))
        ->setDescription(t('OAuth access secret key.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 40)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 50,
        ]);

    $fields['rsa_path_key'] = BaseFieldDefinition::create('string')
        ->setLabel(t('RSA path key'))
        ->setDescription(t('Key for building path where the private key to find.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 32)
        ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 60,
        ]);

    $fields['date_accepted'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Accepted'))
        ->setDescription(t('The date on which the request has been accepted.'));

    $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Requested on'))
        ->setDescription(t('The date on which the service has been requested.'));

    $fields['Last change'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Last change'))
        ->setDescription(t('The date of the last change.'));

    return $fields;
  }

}
