<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\Form\XtOAuthClientDeleteForm.
 */

namespace Drupal\sxt_oauth\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Deletion confirmation form for slogitem entity.
 */
class XtOAuthAcceptedDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_oauth_accepted_confirm_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.xtouth_accepted.collection');
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

}
