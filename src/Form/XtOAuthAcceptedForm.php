<?php

/**
 * @file
 * Contains \Drupal\sxt_oauth\Form\XtOAuthClientForm.
 */

namespace Drupal\sxt_oauth\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form handler for the oauth client edit forms.
 *
 * @internal
 */
class XtOAuthAcceptedForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $client = $this->entity;

//    $form['#title'] = t('Add OAuth acceptedXXX');

    return $form;



    // Try to restore from temp store, this must be done before calling
    // parent::form().
//            $store = $this->tempStoreFactory->get('node_preview');
//
//            // Attempt to load from preview when the uuid is present unless we are
//            // rebuilding the form.
//            $request_uuid = \Drupal::request()->query->get('uuid');
//            if (!$form_state->isRebuilding() && $request_uuid && $preview = $store->get($request_uuid)) {
//              /** @var $preview \Drupal\Core\Form\FormStateInterface */
//
//              $form_state->setStorage($preview->getStorage());
//              $form_state->setUserInput($preview->getUserInput());
//
//              // Rebuild the form.
//              $form_state->setRebuild();
//
//              // The combination of having user input and rebuilding the form means
//              // that it will attempt to cache the form state which will fail if it is
//              // a GET request.
//              $form_state->setRequestMethod('POST');
//
//              $this->entity = $preview->getFormObject()->getEntity();
//              $this->entity->in_preview = NULL;
//
//              $form_state->set('has_been_previewed', TRUE);
//            }

    /** @var \Drupal\node\NodeInterface $node */
//    $node = $this->entity;

//              if ($this->operation == 'edit') {
//                $form['#title'] = $this->t('<em>Edit @type</em> @title', [
//                  '@type' => node_get_type_label($node),
//                  '@title' => $node->label()
//                ]);
//              }
//
//              // Changed must be sent to the client, for later overwrite error checking.
//              $form['changed'] = [
//                '#type' => 'hidden',
//                '#default_value' => $node->getChangedTime(),
//              ];
//
//              $form = parent::form($form, $form_state);
//
//              $form['advanced']['#attributes']['class'][] = 'entity-meta';
//
//              $form['meta'] = [
//                '#type' => 'details',
//                '#group' => 'advanced',
//                '#weight' => -10,
//                '#title' => $this->t('Status'),
//                '#attributes' => ['class' => ['entity-meta__header']],
//                '#tree' => TRUE,
//                '#access' => $this->currentUser->hasPermission('administer nodes'),
//              ];
//              $form['meta']['published'] = [
//                '#type' => 'item',
//                '#markup' => $node->isPublished() ? $this->t('Published') : $this->t('Not published'),
//                '#access' => !$node->isNew(),
//                '#wrapper_attributes' => ['class' => ['entity-meta__title']],
//              ];
//              $form['meta']['changed'] = [
//                '#type' => 'item',
//                '#title' => $this->t('Last saved'),
//                '#markup' => !$node->isNew() ? format_date($node->getChangedTime(), 'short') : $this->t('Not saved yet'),
//                '#wrapper_attributes' => ['class' => ['entity-meta__last-saved']],
//              ];
//              $form['meta']['author'] = [
//                '#type' => 'item',
//                '#title' => $this->t('Author'),
//                '#markup' => $node->getOwner()->getUsername(),
//                '#wrapper_attributes' => ['class' => ['entity-meta__author']],
//              ];
//
//              $form['status']['#group'] = 'footer';
//
//              // Node author information for administrators.
//              $form['author'] = [
//                '#type' => 'details',
//                '#title' => t('Authoring information'),
//                '#group' => 'advanced',
//                '#attributes' => [
//                  'class' => ['node-form-author'],
//                ],
//                '#attached' => [
//                  'library' => ['node/drupal.node'],
//                ],
//                '#weight' => 90,
//                '#optional' => TRUE,
//              ];
//
//              if (isset($form['uid'])) {
//                $form['uid']['#group'] = 'author';
//              }
//
//              if (isset($form['created'])) {
//                $form['created']['#group'] = 'author';
//              }
//
//              // Node options for administrators.
//              $form['options'] = [
//                '#type' => 'details',
//                '#title' => t('Promotion options'),
//                '#group' => 'advanced',
//                '#attributes' => [
//                  'class' => ['node-form-options'],
//                ],
//                '#attached' => [
//                  'library' => ['node/drupal.node'],
//                ],
//                '#weight' => 95,
//                '#optional' => TRUE,
//              ];
//
//              if (isset($form['promote'])) {
//                $form['promote']['#group'] = 'options';
//              }
//
//              if (isset($form['sticky'])) {
//                $form['sticky']['#group'] = 'options';
//              }
//
//              $form['#attached']['library'][] = 'node/form';
//
//              return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
//    $node = $this->entity;
//    $preview_mode = $node->type->entity->getPreviewMode();
//
//    $element['submit']['#access'] = $preview_mode != DRUPAL_REQUIRED || $form_state->get('has_been_previewed');
//
//    $element['preview'] = [
//      '#type' => 'submit',
//      '#access' => $preview_mode != DRUPAL_DISABLED && ($node->access('create') || $node->access('update')),
//      '#value' => t('Preview'),
//      '#weight' => 20,
//      '#submit' => ['::submitForm', '::preview'],
//    ];
//
//    $element['delete']['#access'] = $node->access('delete');
//    $element['delete']['#weight'] = 100;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {



//                $node = $this->entity;
//                $insert = $node->isNew();
//                $node->save();
//                $node_link = $node->link($this->t('View'));
//                $context = ['@type' => $node->getType(), '%title' => $node->label(), 'link' => $node_link];
//                $t_args = ['@type' => node_get_type_label($node), '%title' => $node->link($node->label())];
//
//                if ($insert) {
//                  $this->logger('content')->notice('@type: added %title.', $context);
//                  drupal_set_message(t('@type %title has been created.', $t_args));
//                }
//                else {
//                  $this->logger('content')->notice('@type: updated %title.', $context);
//                  drupal_set_message(t('@type %title has been updated.', $t_args));
//                }
//
//                if ($node->id()) {
//                  $form_state->setValue('nid', $node->id());
//                  $form_state->set('nid', $node->id());
//                  if ($node->access('view')) {
//                    $form_state->setRedirect(
//                      'entity.node.canonical',
//                      ['node' => $node->id()]
//                    );
//                  }
//                  else {
//                    $form_state->setRedirect('<front>');
//                  }
//
//                  // Remove the preview entry from the temp store, if any.
//                  $store = $this->tempStoreFactory->get('node_preview');
//                  $store->delete($node->uuid());
//                }
//                else {
//                  // In the unlikely case something went wrong on save, the node will be
//                  // rebuilt and node form redisplayed the same way as in preview.
//                  drupal_set_message(t('The post could not be saved.'), 'error');
//                  $form_state->setRebuild();
//                }
  }

}
