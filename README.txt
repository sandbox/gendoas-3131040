sxt_oauth - ....Customized MediaWiki Extension OAuth


?????
***************************************

Utilized "mediawiki/oauthclient"
PHP [OAuth][] client for use with [Wikipedia][] and other MediaWiki-based
wikis running the [OAuth extension][].

Customized for own namespaces


Looks like a dead end, use oauth2_server and oauth2_client instead

    - https://www.drupal.org/project/oauth2_server
    - https://www.drupal.org/project/oauth2_client

Documentation

    - https://www.drupal.org/docs/8/modules/oauth2-client
    - https://git.drupalcode.org/project/oauth2_client/blob/8.x-2.x/README.md